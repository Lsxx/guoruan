﻿<!DOCTYPE html>
<%@ Import Namespace="EmptyProjectNet40_FineUI" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>采购协同平台</title>
    <link href="Contexts/Html/pages/css/base.css" rel="stylesheet">
    <link href="Contexts/Html/pages/css/login/login.css" rel="stylesheet">
</head>
<body>
    <div class="login-hd">
        <div class="left-bg"></div>
        <div class="right-bg"></div>
        <div class="hd-inner">
            <span class="logo"></span>
            <span class="split"></span>
            <span class="sys-name">采购协同平台</span>
        </div>
    </div>
    <div class="login-bd">
        <div class="bd-inner">
            <div class="inner-wrap">
                <div class="lg-zone">
                    <div class="lg-box">
                        <div class="lg-label">
                            <h4>用户登录</h4>
                        </div>
                        <div id="error" class="alert alert-error">
                            <i class="iconfont">&#xe62e;</i>
                            <span id="errorInfo"></span>
                        </div>
                        <form id="formLogin" action="/api/login.ashx" method="post">
                            <div class="lg-username input-item clearfix">
                                <i class="iconfont">&#xe60d;</i>
                                <input id="tboxAccount" name="tboxAccount" type="text" placeholder="账号/邮箱">
                            </div>
                            <div class="lg-password input-item clearfix">
                                <i class="iconfont">&#xe634;</i>
                                <input id="tboxPasswd" name="tboxPasswd" type="password" placeholder="请输入密码">
                            </div>
                            <div class="lg-check clearfix">
                                <div class="input-item">
                                    <i class="iconfont">&#xe633;</i>
                                    <input id="tboxVerificationCode" name="tboxVerificationCode" type="text" placeholder="验证码">
                                </div>
                                <span class="check-code">
                                    <img id="imgVcode" src="captcha/captcha.ashx?w=115&h=43" alt="点击刷新验证码" title="点击刷新验证码" />
                                </span>
                            </div>
                            <div class="tips clearfix">
                                <label>
                                    <input id="cboxIsMemory" type="checkbox">记住用户名</label>
                                <a href="javascript:;" class="register">立即注册</a>
                                <a href="javascript:;" class="forget-pwd">忘记密码？</a>
                            </div>
                            <div class="enter">
                                <a id="btnBuyerLogin" href="javascript:;" class="purchaser">安全登录</a>
                                <%--<a id="btnSupplierLogin" href="javascript:;" class="supplier">供应商登录</a>--%>
                            </div>
                            <input id="tboxImpExp" name="tboxImpExp" type="hidden" />
                        </form>
                    </div>
                </div>
                <div class="lg-poster"></div>
            </div>
        </div>
    </div>
    <div class="login-ft">
        <div class="ft-inner">
            <div class="about-us">
                <a href="javascript:;">关于我们</a>
                <a href="javascript:;">法律声明</a>
                <a href="javascript:;">服务条款</a>
                <a href="javascript:;">联系方式</a>
            </div>
            <div class="address">地址：山东省济南市软件园&nbsp;邮编：210019&nbsp;&nbsp;Copyright&nbsp;©&nbsp;2016&nbsp;-&nbsp;2017&nbsp;easyui-专注于框架设计&nbsp;版权所有</div>
            <div class="other-info">建议使用IE8及以上版本浏览器&nbsp;鲁ICP备&nbsp;012345678号&nbsp;E-mail：admin@easyui.com</div>
        </div>
    </div>
    <script src="Contexts/Html/custom/jquery.min.js"></script>
    <script type="text/javascript">
        //初始化
        var loginModel = {
            tAccount: '',
            tPasswd: '',
            tVcode: '',
            cIsMemory: false,
            ImpExp: ''
        }
        //不显示错误信息
        $('#error').hide();

        // 错误信息   msg:错误提示信息
        function error(msg) {
            $('#errorInfo').text(msg);
            $('#error').show();
        }

        // 获取登录信息
        function getLoginInfo() {
            loginModel.tAccount = $('#tboxAccount').val();
            loginModel.tPasswd = $('#tboxPasswd').val();
            loginModel.tVcode = $('#tboxVerificationCode').val();
            loginModel.cIsMemory = $("#cboxIsMemory").is(':checked');
            loginModel.ImpExp = $("#tboxImpExp").val();

            if (loginModel.tAccount === '' || loginModel.tPasswd === '' || loginModel.tVcode === '') {
                error('账号、密码、验证码不能空');
                return false;
            }
            if (loginModel.ImpExp === '') {
                error('登录提交异常');
                return false;
            }
            return true;
        }


        // 点击验证码刷新
        $("#imgVcode").click(function () {
            this.src = "captcha/captcha.ashx?w=115&h=43&token=" + Math.random();
        });

        // 采购商登录验证操作
        $("#btnBuyerLogin").click(function () {
            $('#tboxImpExp').val('Imp');
            $('#error').hide();
            login();
        });
        // 供应商登录验证操作
        $("#btnSupplierLogin").click(function () {
            $('#tboxImpExp').val('Exp');
            $('#error').hide();
            login();
        });
        function login() {
            if (getLoginInfo()) {
                // TODO 测试取值效果使用，正式交付后，请删除
                console.table(loginModel);
                // TODO 测试取值效果使用，正式交付后，请删除


                $.ajax({
                    cache: true,
                    type: "POST",
                    url: '/api/login.ashx',
                    data: loginModel,
                    async: false,
                    error: function (request) {
                        error(request);
                    },
                    success: function (data) {
                        if (!data.IsOk) {
                            // 获取后台登录错误信息处理
                            error(data.Error);
                        } else {
                            alert("登录成功，后台可以直接做页面拦截跳转逻辑");
                        }
                    }
                });
            }
        }
    </script>
</body>
</html>

