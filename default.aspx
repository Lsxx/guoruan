﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="EmptyProjectNet20._default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>FineUI（开源版）空项目</title>
    <link href="Contexts/Html/pages/css/base.css" rel="stylesheet">
    <link href="Contexts/Html/pages/css/platform.css" rel="stylesheet">
    <link rel="stylesheet" href="Contexts/Html/custom/easyui/easyui.css">
    <link href="//cdn.bootcss.com/layer/3.0.1/skin/default/layer.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div id="pf-hd">
            <div class="pf-logo">
                <img src="Contexts/Html/pages/images/main/main_logo.png" alt="logo">
            </div>
            <div class="pf-nav-wrap">
                <div class="pf-nav-ww">
                    <ul class="pf-nav">
                        <li class="pf-nav-item home current" data-menu="sys-manage">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe63f;</span>
                                <span class="pf-nav-title">系统管理</span>
                            </a>
                        </li>
                        <li class="pf-nav-item project" data-menu="org-manage">
                            <a href="javascript:;"  onclick="addTab('组织管理','Contexts/Html/pages/index.html');">
                                <span class="iconfont">&#xe60d;</span>
                                <span class="pf-nav-title">组织管理</span>
                            </a>
                        </li>
                        <li class="pf-nav-item static" data-menu="main-data">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe61e;</span>
                                <span class="pf-nav-title">主数据</span>
                            </a>
                        </li>
                        <li class="pf-nav-item manger" data-menu="supplier-mange">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe620;</span>
                                <span class="pf-nav-title">供应商管理</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="supplier-dev">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe625;</span>
                                <span class="pf-nav-title">供应商开发</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="pur-source">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe64b;</span>
                                <span class="pf-nav-title">采购寻源</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="contract-mange">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe64c;</span>
                                <span class="pf-nav-title">合同管理</span>
                            </a>
                        </li>


                        <li class="pf-nav-item manger" data-menu="pur-source">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe623;</span>
                                <span class="pf-nav-title">示例示例</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="contract-mange">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe646;</span>
                                <span class="pf-nav-title">示例示例示例示例示例</span>
                            </a>
                        </li>
                        <li class="pf-nav-item manger" data-menu="pur-source">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe623;</span>
                                <span class="pf-nav-title">采购寻源</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="contract-mange">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe646;</span>
                                <span class="pf-nav-title">合同管理</span>
                            </a>
                        </li>


                        <li class="pf-nav-item manger" data-menu="pur-source">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe623;</span>
                                <span class="pf-nav-title">示例示例</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="contract-mange">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe646;</span>
                                <span class="pf-nav-title">示例示例示例示例示例示例</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="pur-source">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe623;</span>
                                <span class="pf-nav-title">采购寻源</span>
                            </a>
                        </li>

                        <li class="pf-nav-item manger" data-menu="contract-mange">
                            <a href="javascript:;">
                                <span class="iconfont">&#xe646;</span>
                                <span class="pf-nav-title">合同管理</span>
                            </a>
                        </li>
                    </ul>
                </div>


                <a href="javascript:;" class="pf-nav-prev disabled iconfont">&#xe606;</a>
                <a href="javascript:;" class="pf-nav-next iconfont">&#xe607;</a>
            </div>

            <div class="pf-user">
                <div class="pf-user-photo">
                    <img src="Contexts/Html/pages/images/main/user.png" alt="">
                </div>
                <h4 class="pf-user-name ellipsis">easyui</h4>
                <i class="iconfont xiala">&#xe607;</i>

                <div class="pf-user-panel">
                    <ul class="pf-user-opt">
                        <li>
                            <a href="javascript:;">
                                <i class="iconfont">&#xe60d;</i>
                                <span class="pf-opt-name">用户信息</span>
                            </a>
                        </li>
                        <li class="pf-modify-pwd">
                            <a href="#">
                                <i class="iconfont">&#xe634;</i>
                                <span class="pf-opt-name">修改密码</span>
                            </a>
                        </li>
                        <li class="pf-logout">
                            <a href="javascript:;">
                                <i class="iconfont">&#xe60e;</i>
                                <span class="pf-opt-name">退出</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div id="pf-bd">
            <div id="pf-sider">
                <h2 class="pf-model-name">
                    <span class="iconfont">&#xe64a;</span>
                    <span class="pf-name">组织管理</span>
                    <span class="toggle-icon"></span>
                </h2>

                <ul class="sider-nav">
                    <!-- ko foreach: Class -->
                    <li data-bind="css: isCurrent === 'True' ? 'current' : ''">
                        <a href="javascript:;">
                            <span class="iconfont sider-nav-icon" data-bind="html: icon"></span>
                            <span class="sider-nav-title" data-bind="text: name"></span>
                            <i class="iconfont">&#xe642;</i>
                        </a>
                        <ul class="sider-nav-s">
                            <li onclick="addTab('采购组织1','Contexts/Html/pages/index.html');"><a href="javascript:;">采购组织1</a></li>
                            <li onclick="addTab('采购组织2','Contexts/Html/pages/process.html');"><a href="javascript:;">采购组织2</a></li>
                            <li onclick="addTab('采购组织3','Contexts/Html/pages/providers.html');"><a href="javascript:;">采购组织3</a></li>
                            <li onclick="addTab('采购组织4','Contexts/Html/pages/providers1.html');"><a href="javascript:;">采购组4</a></li>
                        </ul>
                    </li>
                    <!-- /ko -->


                </ul>
            </div>

            <div id="pf-page">
                <div id="pf-page-tab" class="easyui-tabs1" style="width: 100%; height: 100%;">
                    <div title="首页" style="padding: 10px 5px 5px 10px;" data-options="tools: '#p-tools'">
                        <iframe class="page-iframe" src="pages/workbench.aspx" frameborder="no" border="no" height="100%" width="100%" scrolling="auto"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div id="pf-ft">
            <div class="system-name">
                <i class="iconfont">&#xe6fe;</i>
                <span>信息管理系统&nbsp;v1.0</span>
            </div>
            <div class="copyright-name">
                <span>CopyRight&nbsp;2017&nbsp;&nbsp;easyui.com&nbsp;版权所有</span>
                <i class="iconfont">&#xe6ff;</i>
            </div>
        </div>
    </div>
    <div id="mm" class="easyui-menu" style="width: 150px;">
        <div id="mm-tabupdate" name="6">刷新标签 </div>
        <div id="mm-tabclose" name="1">关闭标签</div>
        <div id="mm-tabcloseother"  name="3">除此之外关闭</div>
        <div id="mm-tabcloseright" name="4">关闭右侧全部</div>
        <div id="mm-tabcloseleft" name="5">关闭左侧全部</div>
        <div id="mm-tabcloseall" name="2">关闭所有标签</div>
    </div>
    <script type="text/javascript" src="Contexts/Html/custom/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/knockout/3.4.1/knockout-min.js"></script>
    <script type="text/javascript" src="Contexts/Html/custom/jquery.easyui.min.js"></script>
    <script src="//cdn.bootcss.com/layer/3.0.1/layer.min.js"></script>
    <!-- <script type="text/javascript" src="Contexts/Html/js/menu.js"></script> -->
    <script type="text/javascript" src="Contexts/Html/pages/js/main.js"></script>
    <!--[if IE 7]>
      <script type="text/javascript">
          $(window).resize(function(){
              $('#pf-bd').height($(window).height()-76);
          }).resize();
      </script>
    <![endif]-->
    <script>
        var menuViewModel = { Class: ko.observable([]), Node: ko.observable([]) }; var menuData; $.ajax({ type: "GET", url: "/api/menu.ashx", async: false, success: function (a) { menuData = a } }); menuViewModel.Class(menuData); menuViewModel.Node(); ko.applyBindings(menuViewModel); var tabCount = 3; $(".easyui-tabs1").tabs({ tabHeight: 44, onSelect: function (b, a) { var c = $(".easyui-tabs1").tabs("getSelected"); if (c.find("iframe") && c.find("iframe").size()) { c.find("iframe").attr("src", c.find("iframe").attr("src")) } }, onContextMenu: function (b, c, a) { b.preventDefault(); if (a > 0) { $("#mm").menu("show", { left: b.pageX, top: b.pageY }).data("tabTitle", c) } } }); function addTab(c, a) { if ($(".easyui-tabs1").tabs("exists", c)) { $(".easyui-tabs1").tabs("select", c) } else { if ($(".easyui-tabs1").tabs("tabs").length > tabCount) { layer.alert("tabs 最多打开" + tabCount + "个"); return } var b = '<iframe class="page-iframe" src="' + a + '" frameborder="no" border="no" height="100%" width="100%" scrolling="auto"></iframe>'; $(".easyui-tabs1").tabs("add", { title: c, content: b, closable: true }) } } $("#mm").menu({ onClick: function (a) { closeTab(this, a.name) } }); function closeTab(c, k) { var h = $(".easyui-tabs1").tabs("tabs"); var a = []; $.each(h, function (p, q) { var o = $(q).panel("options"); if (o.closable) { a.push(o.title) } }); var l = $(c).data("tabTitle"); var j = $(".easyui-tabs1").tabs("getTabIndex", $(".easyui-tabs1").tabs("getTab", l)); switch (k) { case "1": $(".easyui-tabs1").tabs("close", j); return false; case "2": for (var g = 0; g < a.length; g++) { $(".easyui-tabs1").tabs("close", a[g]) } break; case "3": for (var f = 0; f < a.length; f++) { if (l !== a[f]) { $(".easyui-tabs1").tabs("close", a[f]) } } $(".easyui-tabs1").tabs("select", l); break; case "4": for (var e = j; e < a.length; e++) { $(".easyui-tabs1").tabs("close", a[e]) } $(".easyui-tabs1").tabs("select", l); break; case "5": for (var d = 0; d < j - 1; d++) { $(".easyui-tabs1").tabs("close", a[d]) } $(".easyui-tabs1").tabs("select", l); break; case "6": var m = $(".easyui-tabs1").tabs("getSelected"); var b = $(m.panel("options").content).attr("src"); $(".easyui-tabs1").tabs("update", { tab: m, options: { content: '<iframe class="page-iframe" src="' + b + '" frameborder="no" border="no" height="100%" width="100%" scrolling="auto"></iframe>' } }); break } return true };
    </script>

<%--    <script type="text/javascript">
        // Menu
        var menuViewModel = {
            Class: ko.observable([]),
            Node: ko.observable([])
        };
        var menuData;
        // 获取公告信息
        $.ajax({
            type: "GET",
            url: "/api/menu.ashx",
            async: false,
            success: function (data) {
                menuData = data;
            }
        });
        menuViewModel.Class(menuData);
        menuViewModel.Node();
        ko.applyBindings(menuViewModel);
        
        // Tab相关
        var tabCount = 3;
        $('.easyui-tabs1').tabs({
            tabHeight: 44,
            onSelect: function(title, index) {
                var currentTab = $('.easyui-tabs1').tabs("getSelected");
                if (currentTab.find("iframe") && currentTab.find("iframe").size()) {
                    currentTab.find("iframe").attr("src", currentTab.find("iframe").attr("src"));
                }
            },
            onContextMenu: function(e, title, index) {
                e.preventDefault();
                if (index > 0) {
                    $('#mm').menu('show',
                    {
                        left: e.pageX,
                        top: e.pageY
                    }).data("tabTitle", title);
                }
            }
        });
        
        function addTab(title, url) {
            if ($('.easyui-tabs1').tabs('exists', title)) {
                $('.easyui-tabs1').tabs('select', title);
            } else {
                if ($('.easyui-tabs1').tabs('tabs').length > tabCount) {
                    layer.alert("tabs 最多打开" + tabCount + "个");
                    return;
                }
                var content = '<iframe class="page-iframe" src="' +
                    url +
                    '" frameborder="no" border="no" height="100%" width="100%" scrolling="auto"></iframe>';
                $('.easyui-tabs1').tabs('add',
                {
                    title: title,
                    content: content,
                    closable: true
                });
            }
        }

        //右键菜单click
        $("#mm").menu({
            onClick: function (item) {
                closeTab(this, item.name);
            }
        });
        //邮件tab
        function closeTab(menu, type) {
            var allTabs = $('.easyui-tabs1').tabs('tabs');
            var allTabtitle = [];
            $.each(allTabs,
                function(i, n) {
                    var opt = $(n).panel('options');
                    if (opt.closable)
                        allTabtitle.push(opt.title);
                });
            var curTabTitle = $(menu).data("tabTitle");
            var curTabIndex = $('.easyui-tabs1').tabs("getTabIndex",$('.easyui-tabs1').tabs("getTab", curTabTitle));
            
            switch (type) {
            case "1": //关闭当前
                $('.easyui-tabs1').tabs("close", curTabIndex);
                return false;
            case "2": //全部关闭
                for (var i = 0; i < allTabtitle.length; i++) {
                    $('.easyui-tabs1').tabs('close', allTabtitle[i]);
                }
                break;
            case "3": //除此之外全部关闭
                for (var i1 = 0; i1 < allTabtitle.length; i1++) {
                    if (curTabTitle !== allTabtitle[i1])
                        $('.easyui-tabs1').tabs('close', allTabtitle[i1]);
                }
                $('.easyui-tabs1').tabs('select', curTabTitle);
                break;
            case "4": //当前侧面右边
                for (var i2 = curTabIndex; i2 < allTabtitle.length; i2++) {
                    $('.easyui-tabs1').tabs('close', allTabtitle[i2]);
                }
                $('.easyui-tabs1').tabs('select', curTabTitle);
                break;
                case "5": //当前侧面左边
                for (var i3 = 0; i3 < curTabIndex - 1; i3++) {
                    $('.easyui-tabs1').tabs('close', allTabtitle[i3]);
                }
                $('.easyui-tabs1').tabs('select', curTabTitle);
                break;
                case "6": //刷新
                    var currTab1 = $('.easyui-tabs1').tabs('getSelected');
                    var url = $(currTab1.panel("options").content).attr("src");
                    $('.easyui-tabs1').tabs("update", {
                        tab: currTab1,
                        options: {
                            content: '<iframe class="page-iframe" src="' + url + '" frameborder="no" border="no" height="100%" width="100%" scrolling="auto"></iframe>'
                        }
                    });
                break;
            }
            return true;
        }
    </script>--%>
</body>
</html>
