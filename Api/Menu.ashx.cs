﻿using System.Data;
using System.Web;
using EmptyProjectNet40_FineUI.Api.Helpers;

namespace EmptyProjectNet40_FineUI.Api
{



    /// <summary>
    /// menu 的摘要说明
    /// </summary>
    public class Menu : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            // Todo 导航菜单分类
            var menuClass = new DataTable();
            menuClass.Columns.Add(new DataColumn("id"));
            menuClass.Columns.Add(new DataColumn("name"));
            //是否展开 
            menuClass.Columns.Add(new DataColumn("isCurrent"));
            // ICON  ： &#xe642;
            menuClass.Columns.Add(new DataColumn("icon"));

            for (int i = 0; i < 5; i++)
            {
                var dr = menuClass.NewRow();
                dr[0] = i;
                dr[1] = "菜单分类" + i;
                dr[2] = i == 1;
                dr[3] = "&#xe642;";
                menuClass.Rows.Add(dr);
            }

            ////Todo 导航菜单分类下的功能节点
            //var menuNode = new DataTable();
            //// 父菜单分类 ID
            //menuNode.Columns.Add(new DataColumn("classId"));
            //// 功能节点名称
            //menuNode.Columns.Add(new DataColumn("name"));
            ////菜单节点的 URL 
            //menuNode.Columns.Add(new DataColumn("url"));

            //for (var j = 0; j < menuClass.Rows.Count; j++)
            //{
            //    for (int i = 0; i < 4; i++)
            //    {
            //        var dr = menuNode.NewRow();
            //        dr[0] = menuClass.Rows[j][0];
            //        dr[1] = "功能节点" + i;
            //        dr[2] = "/info/" + i;
            //        menuNode.Rows.Add(dr);
            //    }
            //}

            HttpContext.Current.Response.Write(menuClass.ToJson());
            HttpContext.Current.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}