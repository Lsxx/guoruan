﻿using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;

namespace EmptyProjectNet40_FineUI.Api
{
    /// <summary>
    /// Login 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class Login : IHttpHandler, IReadOnlySessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            var froms = context.Request.Form;
            // 获取不到前台提交数据
            if (froms.Count == 0) throw new NoNullAllowedException("提交数据为Null");

            // 输入的账号
            var account = froms["tAccount"];
            // 输入的密码
            var password = froms["tPasswd"];
            // 输入的验证码
            var verificationCode = froms["tVcode"];
            // 是否勾选记住账号  true:勾选  false:未勾选
            var isMemory = froms["cIsMemory"];
            // 点击 "采购商登录:Imp" or "供应商登录:Exp"
            var impExp = froms["ImpExp"];

            var result = new ResultLogin();
            if (!verificationCode.Equals(context.Session["CaptchaImageText"].ToString()))
            {
                // 验证码输入有误
                result.Error = "验证码输入有误";
                result.IsOk = false;
                System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonStr = ser.Serialize(result);
                HttpContext.Current.Response.Write(jsonStr);
                HttpContext.Current.Response.End();
            }


            if (impExp.Equals("Imp")) //采购商登录
            {

            }
            else if (impExp.Equals("Exp")) //供应商登录
            {

            }
            else
            {
                //不是点击登录按钮的非法登录
            }

            result.IsOk = true;
            result.LoginDateTime = DateTime.Now;
            System.Web.Script.Serialization.JavaScriptSerializer ser1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonStr1 = ser1.Serialize(result);
            HttpContext.Current.Response.Write(jsonStr1);
            HttpContext.Current.Response.End();
        }
        /// <summary>
        /// 登陆返回值信息
        /// </summary>
        public class ResultLogin
        {
            public bool IsOk { get; set; }
            public string Error { get; set; }
            public DateTime LoginDateTime { get; set; }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}