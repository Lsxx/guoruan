﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using EmptyProjectNet40_FineUI.Api.Helpers;

namespace EmptyProjectNet40_FineUI.Api.WorkBench
{

    /// <summary>
    /// OpenInfo 的摘要说明
    /// </summary>
    public class OpenInfo : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Charset = "utf-8";

            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("title"));
            dt.Columns.Add(new DataColumn("date"));
            dt.Columns.Add(new DataColumn("link"));
            dt.Columns.Add(new DataColumn("isNew"));

            for (int i = 0; i < 5; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = "公告标题信息" + i;
                dr[1] = "01-01";
                dr[2] = "/Info/" + i;
                dr[3] = i < 2;
                dt.Rows.Add(dr);
            }
            context.Response.Write(dt.ToJson());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}