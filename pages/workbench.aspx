﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="workbench.aspx.cs" Inherits="EmptyProjectNet40_FineUI.pages.workbench" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Contexts/Html/pages/css/base.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Contexts/Html/custom/easyui/easyui.css" />
    <link rel="stylesheet" href="../Contexts/Html/pages/css/workbench.css" />
     <link href="//cdn.bootcss.com/layer/3.0.1/skin/default/layer.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <div id="bd">
                    <div class="bd-content">
                        <div class="right-zone">
                            <div class="inform item-box">
                                <div class="inform-hd">
                                    <label>通知公告</label>
                                    <a href="/api/workbench/openinfo.ashx">更多<span>&gt;</span></a>
                                </div>
                                <ul id="listInfo">
                                   <%-- <li>
                                        <span></span>
                                        <a href="javascript:;" class="ellipsis">easyui信息管理<i></i></a>
                                        <label>04-13</label>
                                    </li>
                                    <li>
                                        <span></span>
                                        <a href="javascript:;" class="ellipsis">光电获土耳其最大固网</a>
                                        <label>04-12</label>
                                    </li>--%>
                                </ul>
                            </div>
                        </div>
                        <div class="center-part">
                            <div class="center-items todo">
                                <ul class="work-items clearfix">
                                    <li>
                                        <div class="work-inner">
                                            <div class="work-item green">
                                                <i class="iconfont">&#xe61f;</i>
                                                <span class="num">14&nbsp;<span class="unit">个</span></span>
                                                <label>待办未处理</label>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="work-inner">
                                            <div class="work-item red">
                                                <i class="iconfont">&#xe622;</i>
                                                <span class="num">6&nbsp;<span class="unit">条</span></span>
                                                <label>预警信息未读</label>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="work-inner">
                                            <div class="work-item yellow">
                                                <i class="iconfont">&#xe61d;</i>
                                                <span class="num">9&nbsp;<span class="unit">封</span></span>
                                                <label>邮件未读</label>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="work-inner">
                                            <div class="work-item blue">
                                                <i class="iconfont">&#xe621;</i>
                                                <span title="2000,00万" class="num">2000,00&nbsp;<span class="unit">万</span></span>
                                                <label>我的询价金额</label>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="work-inner">
                                            <div class="work-item purple">
                                                <i class="iconfont">&#xe61e;</i>
                                                <span title="2000,00万" class="num">100,00&nbsp;<span class="unit">万</span></span>
                                                <label>已完成的合同金额</label>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="work-inner">
                                            <div class="work-item gray">
                                                <i class="iconfont">&#xe620;</i>
                                                <span class="num">10&nbsp;<span class="unit">个</span></span>
                                                <label>供应商开发</label>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ft"></div>
            </div>
            <!-- 代办事件的列表面板 -->
            
            <script type="text/javascript" src="../Contexts/Html/custom/jquery.min.js"></script>
            <script type="text/javascript" src="../Contexts/Html/custom/jquery.easyui.min.js"></script>
            <script src="//cdn.bootcss.com/layer/3.0.1/layer.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    function isNew(bool) {
                        if (bool === "True") {
                            return "<i></i>";
                        }
                        return '';
                    }

                    // 获取公告信息
                    $.ajax({
                        type: "GET",
                        url: "/api/workbench/openinfo.ashx",
                        success: function (data) {
                            var infoHtml = "";
                            if (data == null) {
                                infoHtml = "公告信息加载中...";
                            } else {
                                for (var i = 0; i < data.length; i++) {
                                    infoHtml += '<li><span></span><a href="' + data[i].link + '" class="ellipsis">' + data[i].title + isNew(data[i].isNew) + '</a><label>' + data[i].date + '</label></li>';
                                }
                            }
                            $("#listInfo").html(infoHtml);
                        }
                    });

                    //我的待办点击事件
                    $(document).on('click', '.work-item.green', function (event) {
                        parent.layer.open({
                            type: 1,
                            //skin: 'layui-layer-rim', //加上边框
                            area: ['480px', '550px'], //宽高
                            content: '<div class="todo-panel"><div class="todo-title"><i class="iconfont">&#xe61f;</i><span class="num">14&nbsp;<span class="unit">个</span></span><label>待办未处理</label></div><div class="todo-items"><ul><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理<i></i></a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>10条</span>供应商开发申请未处理<i></i></a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>0条</span>供应商开发申请未处理，请及时审批<i></i></a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>1条</span>供应商开发申请未处理，请及时审批</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>4条</span>供应商开发申请未处理，请及时审批</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>6条</span>供应商开发申请未处理，请及时审批，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，未处理</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>开发申请未处理，请及时审批，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，未处理会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，会导致失效</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批，未处理</a><label>04-13</label></li><li><span></span><a href="javascript:;" class="ellipsis">您有<span>2条</span>供应商开发申请未处理，请及时审批</a><label>04-13</label></li></ul></div></div>'
                        });
                        event.stopPropagation();
                    });
                });
                //公开附件tab事件处理
                $(".attached-tab").on("click",
                    "a",
                    function () {
                        $(this).closest(".attached-tab").find("a").removeClass("current");
                        $(this).addClass("current");
                        $(this).closest(".attached").find("ul").addClass("hide");
                        $(this).closest(".attached").find("ul." + $(this).attr("attached")).removeClass("hide");
                    });

            </script>
        </div>
    </form>
</body>
</html>
